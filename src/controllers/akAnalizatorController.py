import os
import sys

from PyQt5 import QtWidgets, QtGui
from PyQt5.QtWidgets import QMessageBox

from src.akFunctions import AkFunctions
from src.controls.akFileDialog import AkFileDialog
from src.data.akAnalizator import AkAnalizator
from src.data.akAnalysis import AkAnalysisType
from src.data.akTypes import AkSelectionMethod
from src.data.akInstrument import AkInstrument
from src.data.akSymbol import AkSymbol
from src.models.akListModel import AkListModel
from src.views.ui_analizator_main import Ui_analizator_main_view


class AkAnalizatorController(QtWidgets.QMainWindow, Ui_analizator_main_view):
    def __init__(self):
        super(AkAnalizatorController, self).__init__()
        super(AkAnalizatorController, self).setupUi(self)

        self._setup_ui()

        self._model = AkListModel()
        self._selected_symbol = None
        self.update_enable_status()

        self.symbols_list_view.setModel(self._model)
        self.calculate_button.setEnabled(self._model.rowCount() > 0)

        # сигнали компонент
        self.action_output_folder.triggered.connect(self.on_open_output_folder)
        self.load_button.clicked.connect(self.on_load_data_button)
        self.calculate_button.clicked.connect(self.on_calculate_button)
        self.delete_selected_symbol_button.clicked.connect(self.on_delete_selected_button)
        self.clear_symbols_button.clicked.connect(self.on_clear_button)
        self.symbols_list_view.clicked.connect(self.on_select_instrument)

        self.action_exit.triggered.connect(self.on_exit_application)

        #self.single_period_edit.textChanged.connect(self.on_check_line_edit_state)
        #self.range_periods_from_edit.textChanged.connect(self.on_check_line_edit_state)
        #self.range_periods_to_edit.textChanged.connect(self.on_check_line_edit_state)

        self.all_periods_rbutton.clicked.connect(self.on_all_periods_rbutton)
        self.range_periods_rbutton.clicked.connect(self.on_range_period_rbutton)
        self.single_period_rbutton.clicked.connect(self.on_single_period_rbutton)

    def _setup_ui(self):
        validator = QtGui.QIntValidator()

        self.single_period_edit.setValidator(validator)
        self.range_periods_from_edit.setValidator(validator)
        self.range_periods_to_edit.setValidator(validator)

    def on_check_line_edit_state(self):
        sender = self.sender()
        validator = sender.validator()

        state = validator.validate(sender.text(), 0)[0]
        if state == QtGui.QValidator.Acceptable:
            color = '#ffffff'  # '#c4df9b'  # green
        else:
            color = '#f6989d'  # red
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def on_single_period_rbutton(self, enabled):
        if enabled:
            self.period_analysis_chb.setChecked(True)
            self._disable_all_components()
            self._setEnabled(True, [self.single_period_label, self.single_period_edit])

    def on_range_period_rbutton(self, enabled):
        if enabled:
            self.period_analysis_chb.setChecked(True)
            self._disable_all_components()

            enable_components = [self.range_periods_from_label, self.range_periods_from_edit,
                                 self.range_periods_to_label, self.range_periods_to_edit]
            self._setEnabled(True, enable_components)

    def on_all_periods_rbutton(self, enabled):
        if enabled:
            self.period_analysis_chb.setChecked(True)
            self._disable_all_components()

    def _disable_all_components(self):
            disable_components = [self.range_periods_from_label,
                                  self.range_periods_from_edit,
                                  self.range_periods_to_label,
                                  self.range_periods_to_edit,
                                  self.single_period_label,
                                  self.single_period_edit]

            self._setEnabled(False, disable_components)

    def _setEnabled(self, value, components=list()):
        for component in components:
            component.setEnabled(value)

    def on_open_output_folder(self):
        output_path = os.path.dirname(os.path.abspath(__file__))
        command = 'explorer.exe' + output_path
        os.system(command)

    def on_delete_selected_button(self):
        """
        Метод видаляє виділений елемент зі списку символів.
        """
        selected_index = self.symbols_list_view.selectedIndexes()[0]

        answer = QMessageBox.question(self, "Delete", "Are you sure you want to delete selected symbol?",
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if answer == QMessageBox.Yes:
            self._model.removeRows(selected_index.row(), 1)
            self.update_enable_status()


    def on_clear_button(self):
        """
        Метод видаляє всі елементи зі списку символів.
        """

        answer = QMessageBox.question(self, "Delete all", "Are you sure you want to delete all symbols?",
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if answer == QMessageBox.Yes:
            self._model.removeRows(0, self._model.rowCount())
            self.update_enable_status()

    def on_select_instrument(self, selection):
        self._selected_symbol = self._model.itemData(selection)
        self.statusbar.showMessage("Rows count: " + str(self._selected_symbol.count()), 5000)

        self._set_default_values()

    def _set_default_values(self):
        self.single_period_edit.setText("1")
        self.range_periods_from_edit.setText("1")
        self.range_periods_to_edit.setText(str(self._selected_symbol.count()))

    def on_load_data_button(self):
        """
        Метод завантажує csv файли у список інструментів.
        """

        # створюємо діалогове вікно імпорта csv файлів
        import_dialog = AkFileDialog()

        selected_files = []
        if import_dialog.exec_() == QtWidgets.QDialog.Accepted:
            selected_files = import_dialog.selectedFiles()
        try:
            if selected_files:
                symbols = []
                for file in selected_files:
                    # повертаємо коротке ім"я файлу
                    name = AkFunctions.get_short_name(file)

                    # якщо файл з таким іменем вже у списку, то ігноруємо
                    if name in self._model.get_names():
                        continue

                    # зчитуємо дані csv файла
                    headers, data = AkFunctions.load_cvs(file)

                    # додаємо новий символ у список символів
                    symbols.append(AkSymbol(name, data, headers))

                if (symbols):
                    # якщо було добавлено нові символи, то добавляємо їх у модель даних
                    self._model.insertRows(0, len(symbols), symbols)
                    self.update_enable_status()

                    index = self.symbols_list_view.model().index(0, 0)
                    self.symbols_list_view.setCurrentIndex(index)
                    self.on_select_instrument(index)


        except Exception as e:
            print("Invalid format:", sys.exc_info()[0])
            QMessageBox.warning(self, "Invalid .csv format", "The file, you've tried to import has invalid format.", QMessageBox.Ok)


    def on_calculate_button(self):
        if self._selected_symbol is None:
            self.statusbar.showMessage("No instrument selected", 3000)
            return


        instrument = self.create_instrument()

        try:

            if instrument:
                analizator = AkAnalizator(instrument)
                analizator.do_analyze()

            QMessageBox.information(self, "Done", "Calculations completed successfully." + "\n" + "Check output files.", QMessageBox.Ok)

        except Exception as e:
            print("Error:", str(e))
            QMessageBox.warning(self, "Failed", "Calculations failed!", QMessageBox.Ok)

    def create_instrument(self):
        # зчитуємо вибрані типи аналізів
        analyzes = self.read_analyzes()

        # зчитуємо значення періодів
        period_values = self.read_period_values()

        if not analyzes or not period_values:
            return

        name = self._selected_symbol.name()
        data = self._selected_symbol.data()
        headers = self._selected_symbol.headers()

        method = self.read_method(self.method_combobox.currentIndex())
        precision = int(self.precision_spinbox.text())

        # створюємо екземпляр класу інструмент
        instrument = AkInstrument(name, [data], analyzes, period_values, method, precision, headers)

        return instrument

    def on_exit_application(self, event):
        reply = QMessageBox.question(self, "Message", "Are you sure you want to quit?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.close()

    def read_analyzes(self):
        analyzes = []
        if self.calendar_analysis_chb.isChecked():
            analyzes.append(AkAnalysisType.Calendar)

        if self.period_analysis_chb.isChecked():
            analyzes.append(AkAnalysisType.Period)

        if self.series_analysis_chb.isChecked():
            analyzes.append(AkAnalysisType.Series)

        return analyzes

    def _valid_line_edit_value(self, value):
        if value == "" or int(value) <= 0:
            return False

        return True

    def read_period_values(self):

        period_values = []
        if self.single_period_rbutton.isChecked():

            if not self._valid_line_edit_value(self.single_period_edit.text()):
                return []

            period_values.append(int(self.single_period_edit.text()))

        elif self.range_periods_rbutton.isChecked():
            from_text_value = self.range_periods_from_edit.text()
            to_text_value = self.range_periods_to_edit.text()

            if (not self._valid_line_edit_value(from_text_value)) or (not self._valid_line_edit_value(to_text_value)):
                return []

            period_values.extend(range(int(from_text_value), int(to_text_value)))

        else:
            period_values.extend(range(1, len(self._selected_symbol.data())))

        return period_values

    def read_method(self, index):
        if index == 1:
            return AkSelectionMethod.OC

        return AkSelectionMethod.CC

    def update_enable_status(self):
        status = self._model.rowCount() > 0

        self.delete_selected_symbol_button.setEnabled(status)
        self.clear_symbols_button.setEnabled(status)
        self.calculate_button.setEnabled(status)

        self.gbox_analysis.setEnabled(status)
        self.gbox_period_values.setEnabled(status)
