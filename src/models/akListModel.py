from PyQt5 import QtCore

class AkListModel(QtCore.QAbstractListModel):
    """
    List model - list of AkSymbols().
    """

    def __init__(self, values = [], parent = None):
        super(AkListModel, self).__init__(parent)
        self._values = values

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self._values)

    def headerData(self, section, orientation, role=None):
        if (role == QtCore.Qt.DisplayRole):
            if (orientation == QtCore.Qt.Vertical):
                return "Values"
            else:
                return QtCore.QVariant(section)

    def data(self, index, role=None):
        if (role == QtCore.Qt.DisplayRole) or (role == QtCore.Qt.EditRole):
            return self._values[index.row()].name()

        if role == QtCore.Qt.ToolTipRole:
            return "Rows count: " + str(self._values[index.row()].count())

    def flags(self, QModelIndex):
        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled

    def setData(self, index, value, role=QtCore.Qt.EditRole):

        if (role == QtCore.Qt.EditRole):
            row = index.row()

            self._values[row].set_name(value)
            self.dataChanged.emit(index, index)
            return True

        if (role == QtCore.Qt.DisplayRole):
            row = index.row()

        return False

    def itemData(self, index):
        return self._values[index.row()]

    def insertRows(self, position, rows, symbols = [], parent=QtCore.QModelIndex()):
        self.beginInsertRows(parent, position, position + rows - 1)

        for i in range(rows):
            self._values.insert(position, symbols[i])

        self.endInsertRows()


    def removeRows(self, position, rows, parent=QtCore.QModelIndex()):
        self.beginRemoveRows(parent, position, position + rows - 1)

        for i in range(rows):
            del self._values[position]

        self.endRemoveRows()

    def get_names(self):
        names_list = []

        for value in self._values:
            names_list.append(value.name())

        return names_list