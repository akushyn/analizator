from typing import IO

from src.data.akAnalysis import AkAnalysisFactory, AkAnalysis
from src.data.akTypes import AkSelectionMethod, AkAnalysisType, AkSourceType, XData
from src.data.akPeriod import AkPeriod
from src.data.akSources import AkSources
from src.data.export.akExport import AkExport

class AkInstrument(object):
    """
    Реалізація класу ІНСТРУМЕНТ
    """
    def __init__(self, name: str, sources: AkSources):
        """
        Ініціалізація екземпляру класу ІНСТРУМЕНТ.
        """

        # ім"я інструмента
        self._name = name

        # список базових даних, завантажених зовні
        self._sources = sources

        # список аналізів інструмента
        self._analyzes = []

    @property
    def name(self) -> str:
        """
        Метод повертає ім"я інструмента.
        """
        return self._name

    @name.setter
    def name(self, name: str):
        """
        Метод встановлює ім"я інструмента.
        """
        self._name = name

    def period_values(self):
        """
        Метод повертає список значень періодів для аналізу інструмента.
        """
        self._period_values = list(range(1, len(self.current_source_data())))

        return self._period_values

    def analyzes(self):
        """
        Метод повертає список об"эктів аналізів інструмента.
        """
        return self._analyzes

    def add_analysis(self, analysis: AkAnalysis):
        """
        Метод додає новий вид аналізу до списку аналізів інструмента.
        """
        if analysis and analysis not in self._analyzes:
            self._analyzes.append(analysis)

    def sources(self) -> AkSources:
        """
        Метод повертає словник базових періодів AkSources().
        """
        return self._sources

    def current_source_type(self) -> AkSourceType:
        return self.sources().current_source_type()

    def current_source_data(self) -> XData:
        return self.sources().current_source_data()

    def set_current_source_type(self, source_type: AkSourceType):
        self.sources().set_current_source_type(source_type)

    def _period_values_output(self, analysis):
        periods_output =  "all"

        period_values = analysis.settings().period_values()
        period_values_count = len(period_values)
        data_count = len(self.current_source_data())

        if period_values_count == 1:
            periods_output = str(period_values[0])
        elif  (period_values_count + 1) < data_count:
            periods_output = str(period_values[0]) + "-" + str(period_values[period_values_count - 1])

        return periods_output


    def calculate(self):
        """
        Вхідна точка обрахунків. Калькуляція необхідної інформація для інструмента.
        """
        source_data = self.sources().current_source_data()

        if not source_data:
            raise Exception("Invalid source data.")

        for analysis in self.analyzes():
            # формуємо базовий період на основі вхідних даних
            x_period = AkPeriod(1, source_data, analysis=analysis)

            analysis.prepare_data(x_period)
            analysis.do_analyze()

    def export_to_file(self, ext='.txt'):
        source_type = self.sources().current_source_type()

        for analysis in self.analyzes():
            if analysis.type() == AkAnalysisType.Calendar:
                file_name = self._get_file_name(analysis, source_type) + ext
            else:
                file_name = self._get_file_name(analysis, source_type) + "_" + self._period_values_output(analysis) + ext

            with open(file_name, "w+") as text_file:  # type: IO[str]
                print('Instrument: ' + self.name, file=text_file)
                print("", file=text_file)
                print("Method: " + analysis.settings().method().name, file=text_file)
                print("Precision: " + str(analysis.settings().precision()), file=text_file)
                AkExport.print_underline(len('Instrument: ' + self.name), "=", text_file=text_file)
                print('', file=text_file)

                analysis.export(text_file)

    def _get_file_name(self, analysis:AkAnalysis, source_type: AkSourceType):
        return self.name + "_" + analysis.type().name + "_" + source_type.name
