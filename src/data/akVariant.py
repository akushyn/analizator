from typing import List

from prettytable import PrettyTable

from src.data import akAmplitude as Amplitude
from src.data import akSequence as Sequence
from src.data import akSeries as Series
from src.data import akStructure as Structure
from src.data.akAmplitude import AkAmplitude
from src.data.akSeries import AkSeriesCategory
from src.data.akStructure import AkStructureEvents
from src.data.akTypes import XData, AkSelectionMethod
from src.data.export.akExport import AkExport


class AkVariant:
    """
    Реалізація класу варіанта періода.
    Кожен період має один або кілька можливих варіантів.

    Базовий період має один варіант - вихідні дані.
    Період 2 має не більше 2х варіантів і тд.

    Максимальна кількість варіантів не може перевищувати значення періоду, до якого варіант належить.

    """

    def __init__(self, number: int, x_data: XData, period):
        # порядковий номер варіанту
        self._number = number

        # період, до якого належить даний варіант
        self._period = period

        # дані варіанту
        self._x_data = x_data

        # амплітуди варіанту періода
        self._amplitudes = []

        # список структур OHLC, OLHC, UN
        self._structures = []

        # зжата послідовність варіанту
        self._sequence = []

        # групи серій, згруповані по значенню серії
        self._series_categories = []

        # словник пар <значення серії> : <кількість серій> категорій варіанта
        self._series_quantities = {}

        self._pretty_table = None

    @property
    def x_data(self) -> XData:
        """
        Таблиця даних DOHLC варіанту.
        """
        return self._x_data

    def number(self) -> int:
        """
        Метод повертає значення номера варіанту в періоді.
        """
        return self._number

    def title(self) -> str:
        """
        Заголовок варіанту періоду.
        """
        return self._period.name + "[" + str(self.number() + 1) + "]"

    def length(self) -> int:
        """
        Метод повертає кількість даних DOHLC варіанту.
        """
        return len(self._x_data)

    def amplitudes(self) -> List[AkAmplitude]:
        """
        Метод повертає амплітуди данних DOHLС варіанту періода для типів 'types'.
        :return: [AkAmplitude(), ...]
        """

        if not self._amplitudes:
            settings = self.analysis_settings()
            self._amplitudes = Amplitude.get_amplitudes(self._x_data, self._period.amplitude_types(), digits=settings.precision())

        return self._amplitudes

    def amplitudes_names(self) -> List[str]:
        """
        Метод повертає список імен амплітуд.
        """
        names = []
        for amplitude in self.amplitudes():
            names.append(amplitude.name)

        return names

    def sequence(self) -> List[int]:
        """
        Метод повертає зжату послідовність варіанту періоду.
        """
        if self._sequence:
            return self._sequence

        base_data = False
        if self.is_base_period():
            base_data = True

        self._sequence = Sequence.get_sequence(self._x_data, self.method(), base_data=base_data)

        return self._sequence

    def period_value(self) -> int:
        """
        Метод повертає значення періоду.
        """
        return self._period.value

    def base_period_data(self) -> XData:
        """
        Метод повертає дані базового періоду.
        """
        return self._period.base_data()

    def is_base_period(self) -> bool:
        """
        Метод перевіряє чи період варіанту є базовим.
        """
        if self._period.is_base():
            return True

        return False

    def analysis_settings(self):
        return self._period.analysis().settings()

    def components_x_data(self) -> XData:
        """
        Кожен період може містити кілька варіантів у зжатому вигляді.
        Даний метод повертає компоненти елементів варіанта у розгорнутому вигляді, використовуючи дані базового періоду.
        :return: [[], ..]
        """

        base_x_data = self.base_period_data()
        period_value = self.period_value()
        variant_number = self.number()

        components = []
        for i in range(self.length()):
            if i == 0 and (variant_number == 0):
                # повертаємо компоненти базового періоду для варіанту
                component = base_x_data[i * period_value: i * period_value + period_value + 1]
            else:
                component = base_x_data[
                            i * period_value + variant_number + 1: i * period_value + period_value + variant_number + 1]

            components.append(component)

        return components

    def structures(self, amplitudes: bool = False, quantities: bool = False) -> List[AkStructureEvents]:
        """
        Метод повертає список подій непорожніх структур OHLC, OLHC, UN варіанту періоду.
        :param amplitudes: Чи рахувати набір амплітуд для варіанту періоду.
        :param quantities: Чи рахувати структури варіанту періоду.
        """
        if self._structures:
            return self._structures

        if self.is_base_period():
            return []

        # дані варіанту
        x_data = self.x_data

        # дані компонент варіанту
        components_x_data = self.components_x_data()

        # повертаємо список непорожніх структур (OHLC, OLHC, UN) варіанту: [AkStructure(),...]
        structures_list = Structure.get_structures_list(x_data, components_x_data, self)

        # рахуємо амплітуди структур
        if amplitudes:
            for s in structures_list:
                s.amplitudes()

        # рахуємо кількість структур (positive, negative, zero) по методу ОС
        if quantities:
            for s in structures_list:
                s.quantities()

        # отримуємо список непорожніх структур OHLC, OLHC, UN варіанту періода
        self._structures = structures_list

        return self._structures

    def series_categories(self, amplitudes: bool = False, structures: bool = False, structure_amplitudes: bool = False,
                          structure_quantities: bool = False) -> List[AkSeriesCategory]:
        """
        Метод повертає список категорій серій.
        Для кожної категорії серій в залежності від вхідних параметрів рахуємо амплітуди,
        структури, амплітуди структур і кількісні величини структур.

        :param amplitudes: bool - рахуємо динаміку серій категорії.
        :param structures: bool - рахуємо структури серій категорії
        :param structure_amplitudes: bool - рахуємо амплітуди структур.
        :param structure_quantities: bool - рахуємо кількісні величини структур
        """
        # якщо категорії серій пораховані раніше
        if self._series_categories:
            return self._series_categories

        # список категорій серій [AkSeriesCategory(),...]
        categories = Series.get_series_categories(variant=self)

        if not categories:
            return []

        # рахуємо попутно динаміку серій |HL|, OC
        if amplitudes:
            for category in categories:
                category.amplitudes()

        # визначаємо структури і динаміку структур
        if structures:
            for category in categories:
                category.structures(amplitudes=structure_amplitudes, quantities=structure_quantities)

        self._series_categories = categories

        return self._series_categories

    def pretty_table(self, x_data: XData, data_include: bool = True, ampl_include: bool = True, table_title: str = "",
                     data_headers: List[str] = None, align: str = 'r'):

        table = PrettyTable()
        table.title = table_title
        table.align = align

        if data_include:

            if data_headers is None:
                data_headers = ["Date", "Open", "High", "Low", "Close"]

            table.field_names = data_headers

            # добавляємо дані у PrettyTable
            for row in x_data:
                table.add_row(row)

        if ampl_include:

            amplitudes_list = self.amplitudes()

            for amplitude in amplitudes_list:

                # форматуємо списки даних у симпатичний вигляд
                points_col = AkExport.format_list(amplitude.points, digits=self.precision())
                percent_col = AkExport.format_list(amplitude.percent, digits=self.precision())

                # добавляємо дані амплітуди : points , percent
                table.add_column(amplitude.name + ", pp", points_col, align=align)
                table.add_column(amplitude.name + ", %", percent_col, align=align)

        return table