from PyQt5 import QtGui, QtCore


class AkPeriodValidator(QtGui.QIntValidator):
    def __init__(self, parent=None):
        QtGui.QIntValidator.__init__(self, parent)

    def validate(self, input_value, pos):
        print("validating")

        if int(input_value) < 1:
            return (QtGui.QValidator.Invalid, pos)

        return QtGui.QValidator.Acceptable