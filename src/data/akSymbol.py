class AkSymbol(object):
    default_headers = ['Date', 'Open', 'High', 'Low', 'Close']
    def __init__(self, name, data, headers=default_headers):
        self._name = name
        self._data = data
        self._headers = headers

    def name(self):
        return self._name

    def set_name(self, name):
        self._name = name

    def data(self):
        return self._data

    def headers(self):
        return self._headers

    def count(self):
        return len(self.data())