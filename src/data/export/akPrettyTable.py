from docutils.nodes import table
from prettytable import PrettyTable

from src.data.akTypes import AkCalculationMode
import heapq, random

def max(table:PrettyTable, col_date, col_points, col_percent, count=1):
    """
    Метод повертає 'count' перших відсортованих мінімумів
    :param col_points:
    :param count: int - кількість відсортованих мінімумів
    """
    table.sortby = "Close"
    table.reversesort = False

    _table = []
    for row in table:
        row.border = False
        row.header = False
        _table.append(row.get_string(fields=["Date", "Low", "Close"]).strip())

    _table = PrettyTable(["Date", "Low", "Close"])
    _table = table[0:6]


    print(_table.get_string(fields=["Date", "Low", "Close"]))


def max1(self, col_name, count=1, date_display=True):
    """
    Метод повертає 'count' перших відсортованих максимумів
    :param col_name:
    :param count: int - кількість відсортованих максимумів
    :return: [float, ...]
    """
    if col_name not in self.field_names:
        return None

    self.sortby = col_name
    self.reversesort = False
    new_table = self[0:count]

    fields = [col_name]
    if date_display:
        fields.insert(0, "Date")

    return new_table.get_string(fields=fields)

def quantitty(self, col_name):
    """
    Метод повертає кількість значень +/-/0 відповідно
    :param col_name:
    :return:
    """
    pass

def intervals(self, step=5, calculation_mode=AkCalculationMode.Points):
    """
    Метод повертає список інтервалів з кроком 'step' відповідно до 'calculation_mode'
    :param step: int - крок інтервалу
    :param calculation_mode: AkCalculationMode: Points, Percent
    :return: [[interval: count], ...] - список інтервалів у табличному представленні
    """
    pass


if __name__ == '__main__':

        x_data = [
                ['1999-12-31', '9212.80', '11568.80', '8994.30', '11497.10'],
                ['2000-12-29', '11501.80', '11750.30', '9651.68', '10786.80'],
                ['2001-12-31', '10790.90', '11350.00', '8062.34', '10021.50'],
                ['2002-12-31', '10021.70', '10673.10', '7197.49', '8341.63'],
                ['2003-12-31', '8342.38', '10462.40', '7416.64', '10453.90'],
                ['2004-12-31', '10452.70', '10868.10', '9708.40', '10783.00'],
                ['2005-12-30', '10783.80', '10984.50', '10000.50', '10717.50'],
                ['2006-12-29', '10718.30', '12529.90', '10661.20', '12463.20'],
                ['2007-12-31', '12459.50', '14198.10', '11939.60', '13264.80'],
                ['2008-12-31', '13261.80', '13279.50', '7449.38', '8776.39'],
                ['2009-12-31', '8772.25', '10580.30', '6469.95', '10428.00'],
                ['2010-12-31', '10430.70', '11625.00', '9614.32', '11577.51'],
                ['2011-12-30', '11577.43', '12876.00', '10404.49', '12217.56'],
                ['2012-12-31', '12221.19', '13661.87', '12035.09', '13104.14'],
                ['2013-12-31', '13104.30', '16588.25', '13104.30', '16576.66'],
                ['2014-12-31', '16572.17', '18103.45', '15340.69', '17823.07'],
                ['2015-12-31', '17823.07', '18351.36', '15370.33', '17425.03'],
                ['2016-12-30', '17405.48', '19987.63', '15450.56', '19762.60'],
                ['2017-12-29', '19872.90', '24876.07', '19677.94', '24719.22'],
                ['2018-12-31', '24809.30', '26616.71', '23344.52', '25326.16']]

        table = PrettyTable(['Date', 'Open', 'High', 'Low', 'Close'])
        for row in x_data:
            table.add_row(row)

        max(table, 'Date', 'Low', 'Close')