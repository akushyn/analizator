from src.akFunctions import AkFunctions
from src.data.akTypes import AkSourceType, XData
from dateutil import parser

class AkSources(object):
    def __init__(self):
        self._source_dict = {
            AkSourceType.D: None,
            AkSourceType.W: None,
            AkSourceType.M: None,
            AkSourceType.Q: None,
            AkSourceType.Y: None
        }

        self._current_source_type = None

    def add_source(self, x_data: XData, source_type=None, current_source=False):

        if (source_type is not None):
            type = source_type
        else:
            # якщо не вказано явно тип секції, то ідентифікуємо по першим двум значенням
            type = self.identify_data_type(x_data[0][0], x_data[1][0])

        # якщо добавляється перше джерело даних, то по замовчуванню буде current
        if self._current_source_type is None:
            self._current_source_type = type
        elif current_source:
            self._current_source_type = type

        self._source_dict[type] = x_data


    def set_current_source_type(self, source_type):
        self._current_source_type = source_type

    def source_types(self):
        """
        Метод повертає список типів вхідних даних.
        :return: []
        """
        return list(self._source_dict.keys())



    def source_data(self, source_type):
        """
        Метод повертає дани сектора за типом.
        :param source_type: AkSourceSection enumeration.
        :return: x_data
        """
        return self._source_dict[source_type]

    def current_source_type(self):
        return self._current_source_type

    def current_source_data(self):
        return self._source_dict[self.current_source_type()]

    def identify_data_type(cls, date0, date1):
        """
        Метод повертає тип даних періоду по двум заданим датам.

        :param: date0. Дата першого елемента даних періоду.
        :param: date1. Дата другого елемента даних періоду.
        :return: AkSectionType().
        """

        dType = None

        # ідемо від найбільшого до найменшого значення по календарю.
        # якщо різниця дат у році, тип даних 'Year'
        if ((date1.year - date0.year) > 0):
            dType = AkSourceType.Y

        # якщо різниця дат у кварталі, то тип даних 'Quarter'
        elif ((AkFunctions.getQuarters(date1) - AkFunctions.getQuarters(date0)) > 0):
            dType = AkSourceType.Q

        # якщо різниця дат у місяці, то тип даних 'Month'
        elif ((date1.month - date0.month) > 0):
            dType = AkSourceType.M

        # якщо різниця дат у тижні, то тип даних 'Week'
        elif ((AkFunctions.getWeeks(date1) - AkFunctions.getWeeks(date0)) > 0):
            dType = AkSourceType.W

        # якщо різниця дат у дні, то тип даних 'Day'
        elif ((date1.day - date0.day) > 0):
            dType = AkSourceType.D

        if dType is None:
            raise Exception("Invalid source type.")

        return dType
