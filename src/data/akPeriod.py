from typing import List

from src.akFunctions import AkFunctions
from src.data.akTypes import AkSelectionMethod
from src.data.akVariant import AkVariant

class AkPeriod:

    def __init__(self, value, x_data, analysis=None, base_period=None):

        # значення періоду
        self._value = value

        # базовий період
        self._basePeriod = base_period

        self._analysis = analysis

        # дані поточного періоду DOHLC
        self._x_data = x_data

        # варіанти періоду
        self._variants = []

    def base_period(self):
        """
        Метод повертає базовий період для поточного періода.

        :return: AkPeriod()
        """
        return self._basePeriod

    def analysis(self):
        return self._analysis

    @property
    def x_data(self):
        """
        Таблиця даних DOHLC періоду.
        :return: [row1...rowN]
        """
        return self._x_data

    @property
    def value(self):
        """
        Значення періоду.
        :return: int.
        """
        return self._value

    @property
    def name(self):
        """
        Ім"я періоду: <тип>+<значення>
        :return: str.
        """
        source_type = self.analysis().instrument.current_source_type()

        return source_type.name + str(self.value)

    def base_data(self):
        """
        Метод повертає дані базового періоду.
        :return: [[],...]
        """
        if self.is_base():
            return self.x_data

        return self.base_period().x_data

    def amplitudes(self):
        """
        Метод повертає список амплітуд данних DOHLС для кожного варіанту періода для типів 'types'.
        """

        # результуючий список списків амплітуд
        amplitudes = []
        for variant in self.variants():
            # рахуємо амплітуди |HL|, OC для варіанту періоду
            amplitudes.append(variant.amplitudes())

        return amplitudes

    def amplitude_types(self):
        """
        Метод повертає список амплітуд періоду.
        :return:
        """
        if self.is_base():
            return [AkSelectionMethod.GAP, AkSelectionMethod.absHL, AkSelectionMethod.OC]

        else:
            return [AkSelectionMethod.absHL, AkSelectionMethod.OC]

    def sequences(self):
        """
        Метод повертає список зжатих послідовностей для кожного варіанту періоду.
        """

        # результуючий список послідовностей
        sequences = []

        for variant in self.variants():
                sequences.append(variant.sequence())

        return sequences

    def structures(self, amplitudes=False, quantities=False):
        """
        Метод повертає список списків непорожніх структур OHLC, OLHC, UN для всіх варіантів періоду.

        :param: amplitudes. Визначає, чи рахувати амплітуди структур чи ні.
        :return: [AkStructure(), ...]
        """

        # якщо період базовий, то структури не рахуємо
        if self.is_base():
            return []

        # список списків структур для всіх варіантів періоду
        structures = []

        for variant in self.variants():
            structures.append(variant.structures(amplitudes, quantities))

        return structures

    def series_categories(self, amplitudes=True, structures=True, structure_amplitudes=True, structure_quantities=True):

        categories_list = []
        for variant in self.variants():
            categories = variant.series_categories(amplitudes, structures, structure_amplitudes, structure_quantities)

            if categories:
                categories_list.append(categories)

        return categories_list

    def is_base(self):
        """
        Метод перевіряє чи період являється базовим, повертає True/False.
        :return:
        """
        if self._value == 1:
            return True

        return False

    def length(self):
        """
        Метод повертає кількість стрічок DOHLC даних періоду.
        :return: int.
        """
        return len(self.x_data)

    def variants(self) -> List[AkVariant]:
        """
        Метод рахує і повертає унікальні варіанти даних періоду.

        :return: [AkVariant(), AkVariant(),...]
        """

        # якщо варіанти вже були пораховані раніше, то повертаємо пораховані
        if self._variants:
            return self._variants

        # якщо період базовий, то варіант один - вихідні дані
        if self._value == 1:
            self._variants = [AkVariant(0, self.x_data, self)]
            return self._variants

        variants = []
        for i in range(self._value):
            variant = []
            j = i
            while j < len(self.x_data):    # цикл по даним DOHLC
                variant.append(self.x_data[j])
                j += self._value

            if variant:
                variants.append(AkVariant(i, variant, self))

        self._variants = variants

        return variants

    def create_period(self, value):
        """
        Метод створює новий період на основі базового через алгоритм згортки даних.

        :param value: int. Значення періоду, який формується.
        :return: AkPeriod(value)
        """

        # якщо період не базовий, згортку не виконуємо
        if self._value > 1:
            return None

        if not self.is_base():
            raise Exception("Invalid base period.")

        # формуємо дані нового періоду
        selection_method = self.analysis().settings().method()
        new_data = self.data_selection(self.base_data(), period_value=value, method=selection_method)

        return AkPeriod(value=value, x_data=new_data, analysis=self.analysis(), base_period=self)

    def data_selection(self, base_x_data, period_value, method):
        """
        Метод формує через згортку дані нового періоду.

        :param base_x_data: Дані базового періоду
        :param period_value: Значення періоду, який формуємо
        :param method: Метод селекції даних.
        :return: [[],...] - Дані сформованого періоду
        """

        # дані нового періоду
        new_data = []

        # метод селекції Close[i] - Close[i-1]
        if method == AkSelectionMethod.CC:
            # в циклі формуємо нові дані
            for i in range(len(base_x_data) - period_value):
                # вибираємо дані з базового періоду
                items = base_x_data[i:i + period_value + 1]

                # формуємо новий рядок через злиття
                item = AkFunctions.fold(items, method)
                new_data.append(item)

        # метод селекції Close[i] - Open[i]
        elif method == AkSelectionMethod.OC:
            # в циклі формуємо нові дані
            for i in range(len(base_x_data) - period_value + 1):
                # вибираємо дані з базового періоду
                items = base_x_data[i:i + period_value]

                # формуємо новий рядок через злиття
                item = AkFunctions.fold(items, method)
                new_data.append(item)

        return new_data

    def title(self):
        """
        Метод повертає заголовок періоду для виводу на екран.
        :return:
        """
        return str("Period: " + self.name + "(" + self.instrument().current_source_type().value + ")")
