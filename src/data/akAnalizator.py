from dateutil import parser

from src.data.akAnalysis import AkAnalysisType, AkAnalysisFactory, AkAnalysis, AkAnalysisSettings
from src.data.akInstrument import AkInstrument
from src.data.akSources import AkSources
from src.data.akTypes import AkSelectionMethod, XData


class AkAnalizator:

    def __init__(self, instrument: AkInstrument=None):

        # інструмент для аналізу
        self._instrument = instrument

    @property
    def instrument(self):
        """
        Метод повертає інструмент аналізу.
        :return: AkInstrument()
        """
        return self._instrument

    @instrument.setter
    def instrument(self, instrument: AkInstrument):
        self._instrument = instrument

    def do_analyze(self):
        if self.instrument:
            self.instrument.calculate()
            self._instrument.export_to_file()


def to_xdata_values(xdata: XData) -> XData:
    """
    Метод ковертує дані .csv файлу у табличне представлення.
    :param xdata: Базові дані періода.
    """

    new_xdata = []
    new_row = []
    for row in xdata:
        for i in range(len(row)):
            if i == 0:
                new_row.append(parser.parse(row[0]))
                continue
            new_row.append(float(row[i]))

        new_xdata.append(new_row)
        new_row = []

    return new_xdata

def configure_analysis(analysis: AkAnalysis):
    if analysis.type() == AkAnalysisType.Calendar:
        configure_calendar(analysis)
    elif analysis.type() == AkAnalysisType.Period:
        configure_period(analysis)
    elif analysis.type() == AkAnalysisType.Series:
        configure_series(analysis)

def configure_calendar(analysis):
    analysis.settings().set_method(AkSelectionMethod.CC)
    analysis.settings().set_precision(2)
    analysis.settings().set_period_values([1])

def configure_period(analysis):
    analysis.settings().set_method(AkSelectionMethod.CC)
    analysis.settings().set_precision(3)

    data_length = len(analysis.instrument.current_source_data())
    analysis.settings().set_period_values(list(range(1, data_length)))

def configure_series(analysis):
    pass


if __name__ == '__main__':
    x_data_year = [
            ['1999-12-31', '9212.80', '11568.80', '8994.30', '11497.10'],
            ['2000-12-29', '11501.80', '11750.30', '9651.68', '10786.80'],
            ['2001-12-31', '10790.90', '11350.00', '8062.34', '10021.50'],
            ['2002-12-31', '10021.70', '10673.10', '7197.49', '8341.63'],
            ['2003-12-31', '8342.38', '10462.40', '7416.64', '10453.90'],
            ['2004-12-31', '10452.70', '10868.10', '9708.40', '10783.00'],
            ['2005-12-30', '10783.80', '10984.50', '10000.50', '10717.50'],
            ['2006-12-29', '10718.30', '12529.90', '10661.20', '12463.20'],
            ['2007-12-31', '12459.50', '14198.10', '11939.60', '13264.80'],
            ['2008-12-31', '13261.80', '13279.50', '7449.38', '8776.39'],
            ['2009-12-31', '8772.25', '10580.30', '6469.95', '10428.00'],
            ['2010-12-31', '10430.70', '11625.00', '9614.32', '11577.51'],
            ['2011-12-30', '11577.43', '12876.00', '10404.49', '12217.56'],
            ['2012-12-31', '12221.19', '13661.87', '12035.09', '13104.14'],
            ['2013-12-31', '13104.30', '16588.25', '13104.30', '16576.66'],
            ['2014-12-31', '16572.17', '18103.45', '15340.69', '17823.07'],
            ['2015-12-31', '17823.07', '18351.36', '15370.33', '17425.03'],
            ['2016-12-30', '17405.48', '19987.63', '15450.56', '19762.60'],
            ['2017-12-29', '19872.90', '24876.07', '19677.94', '24719.22'],
            ['2018-12-31', '24809.30', '26616.71', '23344.52', '25326.16']]

    x_data_quarter = [
            ['6-30-2010', '10857.3', '11258', '9753.84', '9774.02'],
            ['9-30-2010', '9773.27', '10948.88', '9614.32', '10788.05'],
            ['12-31-2010', '10789.72', '11625', '10711.12', '11577.51'],
            ['3-31-2011', '11577.43', '12391.29', '11555.48', '12319.73'],
            ['6-30-2011', '12321.02', '12876', '11862.53', '12414.34'],
            ['9-30-2011', '12412.07', '12753.89', '10597.14', '10913.38'],
            ['12-31-2011', '10912.1', '12328.47', '10404.49', '12217.56'],
            ['3-31-2012', '12221.19', '13289.08', '12221.19', '13212.04'],
            ['6-30-2012', '13211.36', '13338.66', '12035.09', '12880.09'],
            ['9-30-2012', '12879.71', '13653.24', '12492.25', '13437.13'],
            ['12-31-2012', '13437.66', '13661.87', '12471.49', '13104.14'],
            ['3-31-2013', '13104.3', '14585.1', '13104.3', '14578.54'],
            ['6-30-2013', '14578.54', '15542.4', '14434.43', '14909.6'],
            ['9-30-2013', '14911.6', '15709.58', '14760.41', '15129.67'],
            ['12-31-2013', '15132.49', '16588.25', '14719.43', '16576.66'],
            ['3-31-2014', '16572.17', '16573.07', '15340.69', '16457.66'],
            ['6-30-2014', '16457.66', '16977.85', '16015.32', '16826.6'],
            ['9-30-2014', '16826.6', '17350.64', '16334.67', '17042.9'],
            ['12-31-2014', '17040.46', '18103.45', '15855.12', '17823.07'],
            ['3-31-2015', '17823.07', '18288.63', '17037.76', '17776.12'],
            ['6-30-2015', '17778.52', '18351.36', '17576.5', '17619.51'],
            ['9-30-2015', '17638.12', '18137.12', '15370.33', '16284.7'],
            ['12-31-2015', '16278.62', '17977.85', '16013.66', '17425.03'],
            ['3-31-2016', '17405.48', '17790.11', '15450.56', '17685.09'],
            ['6-30-2016', '17661.74', '18167.63', '17063.08', '17929.99'],
            ['9-30-2016', '17924.24', '18668.44', '17713.45', '18308.15'],
            ['12-31-2016', '18279.6', '19987.63', '17883.56', '19762.6'],
            ['3-31-2017', '19872.86', '21169.11', '19677.94', '20663.22'],
            ['6-30-2017', '20665.17', '21535.03', '20379.55', '21349.63'],
            ['9-30-2017', '21392.3', '22419.51', '21279.3', '22405.09'],
            ['12-31-2017', '22423.47', '24876.07', '22416', '24719.22'],
            ['3-31-2018', '24809.35', '26616.71', '23360.29', '24103.11'],
            ['6-30-2018', '24076.6', '25402.83', '23344.52', '24271.41'],
            ['9-30-2018', '24161.53', '26769.16', '24077.56', '26458.31'],
            ['12-31-2018', '26598.36', '26951.81', '21712.53', '23327.46'],
            ['3-31-2019', '23058.61', '26241.42', '22638.41', '26026.32']]


    # створюємо набір вхідних даних
    source_list = AkSources()

    # якщо вхідні дані являються поточними, то current_source=True
    source_list.add_source(to_xdata_values(x_data_year))
    #source_list.add_source(to_xdata_values(x_data_quarter), current_source=True)

    # створюємо тепер інструмент з набором даних
    instrument_ = AkInstrument("DJ", sources=source_list)

    # визначаємо типи аналізів, що вибрав користувач
    analysis_types = [AkAnalysisType.Calendar, AkAnalysisType.Period]
    analyzes_list = []

    for analysis_type in analysis_types:
        analysis = AkAnalysisFactory.create_analysis(analysis_type, instrument_)

        # налаштовуємо кожен аналіз по своєму
        configure_analysis(analysis)
        instrument_.add_analysis(analysis)


    # тепер створюємо аналізатор і вказуємо інструмент аналізу.
    analizator = AkAnalizator()
    analizator.instrument = instrument_

    # виконання аналізу
    analizator.do_analyze()


