from typing import List, Any
from src.data.akTypes import AkSelectionMethod, XData

# Тип значень Point
Point = float

# Тип значень Percent
Percent = float

# значення [Date, Point, Value]
AmplitudeValue = List[Any]


class AkAmplitude(object):
    """
    Реалізація класу 'Амплітуда'. Кожен бар має Open, High, Low, Close величини. Різниця наборів величин визначає
    амплітуду коливань, тобто розмах по величинам показує динаміку в часі.

    Існують різні набори амплітуд, наприклад:
    * OH, HL, LC, OC - набір амплітуд для бичих барів
    * OL, LH, HC, OC - набір амплітуд для медвежих барів.
    """

    def __init__(self, amplitude_type: AkSelectionMethod, values: List[AmplitudeValue]):

        # тип амплітуди
        self._amplitude_type = amplitude_type

        # список списків значеннь амплітуди [Date, Point, Percent]
        self._values = values

    @property
    def amplitude_type(self) -> AkSelectionMethod:
        """
        Метод повертає тип амплітуди.
        """
        return self._amplitude_type

    @property
    def name(self) -> str:
        """
        Метод повертає ім"я амплітуди в класичному математичному виді.
        """
        # повертаємо повне ім"я амплітуди
        amplitude_name = str(self.amplitude_type.name)

        # якщо ім"я містить 'abs', то перетворюємо стрічку виду |HL|
        if 'abs' in amplitude_name:
            return "|" + amplitude_name[3:] + "|"

        return amplitude_name

    @staticmethod
    def headers():
        """
        Метод повертає заголовок для амплітуди.
        """
        return ["Date", "Points", "Percent"]

    @property
    def length(self) -> int:
        """
        Метод повертає довжину даних амплітуди
        :return: int
        """
        return len(self._values)

    @property
    def points(self) -> List[Point]:
        """
        Метод повертає список значень 'points' амплітуди.
        """
        # результуючий вектор
        point_values = []

        # цикл по довжині даних амплітуди, вибираємо 'points' стовпець
        for i in range(self.length):
            point_values.append(self._values[i][1])

        return point_values

    @property
    def percent(self) -> List[Percent]:
        """
        Метод повертає список значень 'percent' амплітуди.
        """
        # результуючий вектор
        percent_values = []

        # цикл по довжині даних амплітуди, вибираємо 'percent' стовпець
        for i in range(self.length):
            percent_values.append(self._values[i][2])

        return percent_values


def get_amplitude(x_data: XData, amplitude_type: AkSelectionMethod, digits: int = 4,
                  base_data: bool = False) -> AkAmplitude:
    """
    Метод повертає амплітуду заданого типу із заданою точністю.

    :param x_data:          Вихідні дані DOHLC
    :param amplitude_type:  Тип амплітуди.
    :param digits:          Точність обрахунків.
    :param base_data:       Чи рахуємо для базових даних
    """
    values = []

    # рахуємо амплітуду GAP
    if amplitude_type == AkSelectionMethod.GAP:
        return get_gap_amplitude(x_data, digits)

    # рахуємо амплітуду Close-Close
    elif amplitude_type == AkSelectionMethod.CC:
        return get_close_close_amplitude(x_data, digits, base_data=base_data)

    # рахуємо одну із амплітуд OH, HL, LC, OL, LH, HC
    else:
        s_idx, e_idx, absolute = _amplitude_type_to_columns(amplitude_type)
        for i in range(len(x_data)):
            date = x_data[i][0]
            if absolute:
                point_value = round(abs(x_data[i][e_idx] - x_data[i][s_idx]), digits)
                percent_value = round(abs(((x_data[i][e_idx] - x_data[i][s_idx]) /
                                           x_data[i][e_idx]) * 100), digits)
            else:
                point_value = round(x_data[i][e_idx] - x_data[i][s_idx], digits)
                percent_value = round(((x_data[i][e_idx] - x_data[i][s_idx]) /
                                       x_data[i][e_idx]) * 100, digits)

            values.append([date, point_value, percent_value])

    return AkAmplitude(amplitude_type, values)


def get_amplitudes(x_data: XData, types: List[AkSelectionMethod], digits: int = 4):
    """
    Метод повертає список амплітуд заданих типів.
    :param x_data:  Базові дані періоду.
    :param types:   Типи амплітуд, які потрібно порахувати.
    :param digits:  Точність обрахунків.
    """
    if not x_data:
        return []

    amplitudes = []
    for amplitude_type in types:
        amplitudes.append(get_amplitude(x_data, amplitude_type, digits))

    return amplitudes


def get_gap_amplitude(x_data: XData, digits: int = 4) -> AkAmplitude:
    """
    Метод повертає амплітуду геп.
    :param      x_data: Дані базового періоду.
    :param      digits: Точність обрахунків
    """
    values = list()

    # gap[0] by default = None
    values.append([x_data[0][0], None, None])

    # Gap[i] = Open[i] - Close[i-1]
    for i in range(1, len(x_data)):
        date = x_data[i][0]
        point_value = round(x_data[i][1] - x_data[i - 1][4], digits)
        percent_value = round(((x_data[i][1] - x_data[i - 1][4]) / x_data[i][1]) * 100, digits)

        values.append([date, point_value, percent_value])

    return AkAmplitude(AkSelectionMethod.GAP, values)


def get_close_close_amplitude(x_data: XData, digits: int = 4, base_data: bool = False) -> AkAmplitude:
    """
    Метод повертає амплітуда Close[i]-Close[i-1].
    :param      x_data:     Дані базового періоду.
    :param      digits:     Точність обрахунків
    :param      base_data:  Чи дані базового періоду.
    """
    values = []

    if base_data:
        # першого елементу нема, тому по замовчуванню None
        values.append([x_data[0][0], None, None])
    else:
        # числове значення різниці
        point0 = round(x_data[0][4] - x_data[0][1], digits)

        # значення різниці в %
        percent0 = round(((x_data[0][4] - x_data[0][1]) / x_data[0][4]) * 100, digits)

        values.append([x_data[0][0], point0, percent0])

    # CC[i] = Close[i] - Close[i-1]
    for i in range(1, len(x_data)):
        # дата
        date = x_data[i][0]

        # числове значення різниці
        point_value = round(x_data[i][4] - x_data[i - 1][4], digits)

        # значення різниці в %
        percent_value = round(((x_data[i][4] - x_data[i - 1][4]) / x_data[i][4]) * 100, digits)

        # добавляємо нове значення у список
        values.append([date, point_value, percent_value])

    return AkAmplitude(AkSelectionMethod.CC, values)


def _amplitude_type_to_columns(amplitude_type: AkSelectionMethod):
    """
    Convert AkSelectionMethod to column numbers.
    :param amplitude_type: AkAmplitudeType
    :return: int(), int(), bool()
    """

    idx_list = [int(x) for x in str(amplitude_type.value)]
    if idx_list[2] == 1:
        absolute = True
    else:
        absolute = False

    return idx_list[0], idx_list[1], absolute
