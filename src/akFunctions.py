import csv
import os

from src.data.akTypes import AkSelectionMethod

class AkFunctions:

    @classmethod
    def toFixed(cls, f, n=0):
        a, b = str(str(float(f))).split('.')

        result = '{}.{}{}'.format(a, b[:n], '0' * (n - len(b)))

        return result

    @classmethod
    def getQuarters(cls, dt):
        return (dt.month - 1) // 3 + 1

    @classmethod
    def getWeeks(cls, dt):
        return dt.isocalendar()[1]

    @classmethod
    def load_cvs(cls, file_name):
        data = []
        with open(file_name, "r") as file_input:
            for row in csv.reader(file_input):
                items = []
                for i in range(5):
                    items.append(row[i]) # = [field for field in row]
                data.append(items)

            headers = data.pop(0)
        return headers, data

    @classmethod
    def get_short_name(cls, path_file_name):
        return os.path.splitext(os.path.basename(path_file_name))[0]

    @classmethod
    def max(cls, x_data):
        '''Get maxValue & rowIndex of x_data.'''
        max_value = x_data[0][2] # High[0]
        row_index = 0
        for j in range(len(x_data)):
            if (float(x_data[j][2]) > float(max_value)):
                max_value = x_data[j][2]
                row_index = j

        return max_value, row_index

    @classmethod
    def min(cls, xData):
        '''Get minValue & rowIndex of x_data.'''
        minValue = xData[0][3] # High[0]
        rowIndex = 0
        for j in range(1, len(xData)):
            if (float(xData[j][3]) < float(minValue)):
                minValue = xData[j][3]
                rowIndex = j

        return minValue, rowIndex

    @classmethod
    def fold(cls, xData, method):

        d = xData[len(xData) - 1][0]
        h, _ = AkFunctions.max(xData)
        l, _ = AkFunctions.min(xData)

        # метод селекції Close[i] - Close[i-1]
        if (method == AkSelectionMethod.CC):
            o = xData[0][4]
            c = xData[len(xData) - 1][4]

        # метод селекції Close[i] - Open[i]
        elif (method == AkSelectionMethod.OC):
            o = xData[0][1]
            c = xData[len(xData) - 1][4]

        return [d, o, h, l, c]
