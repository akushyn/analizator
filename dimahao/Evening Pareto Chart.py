# Get this figure: fig = py.get_figure("https://plot.ly/~samcarlin/19/")
# Get this figure's data: data = py.get_figure("https://plot.ly/~samcarlin/19/").get_data()
# Add data to this figure: py.plot(Data([Scatter(x=[1, 2], y=[2, 3])]), filename ="Evening Pareto Chart", fileopt="extend")
# Get x data of first trace: x1 = py.get_figure("https://plot.ly/~samcarlin/19/").get_data()[0]["x"]

# Get figure documentation: https://plot.ly/python/get-requests/
# Add data documentation: https://plot.ly/python/file-options/

# If you're using unicode in your file, you may need to specify the encoding.
# You can reproduce this figure in Python with the following code!

# Learn about API authentication here: https://plot.ly/python/getting-started
# Find your api_key here: https://plot.ly/settings/api

import plotly.plotly as py
from plotly.graph_objs import *
py.sign_in('username', 'api_key')
trace1 = {
  "x": ["C", "A", "H", "H", "C", "D", "A", "H", "A", "H", "D", "A", "H", "R", "D", "H", "H", "A", "C", "C"], 
  "autobinx": False, 
  "name": "Genre (E)", 
  "type": "histogram", 
  "uid": "d0e7f5", 
  "xbins": {
    "end": 4.5, 
    "size": 1, 
    "start": -0.5
  }, 
  "xsrc": "samcarlin:14:fdbfb5"
}
data = Data([trace1])
layout = {
  "autosize": True, 
  "dragmode": "zoom", 
  "height": 615, 
  "hovermode": "closest", 
  "title": "Evening Pareto Chart", 
  "width": 1166, 
  "xaxis": {
    "autorange": False, 
    "range": [-0.892296636601, 5.2147171542], 
    "title": "Genre (E)", 
    "type": "category"
  }, 
  "yaxis": {
    "autorange": False, 
    "range": [-0.652555497735, 8.34725429923], 
    "title": "Frequency", 
    "type": "category"
  }
}
fig = Figure(data=data, layout=layout)
plot_url = py.plot(fig)