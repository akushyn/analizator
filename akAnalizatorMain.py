import sys
from PyQt5 import QtWidgets
from src.controllers.akAnalizatorController import AkAnalizatorController

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    application = AkAnalizatorController()
    application.show()
    sys.exit(app.exec_())
